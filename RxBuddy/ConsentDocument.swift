//
//  ConsentDocument.swift
//  RxBuddy
//
//  Created by Paul Vu on 2015-11-19.
//  Copyright © 2015 275g5. All rights reserved.
//

import Foundation
import ResearchKit

public var ConsentDocument: ORKConsentDocument {
    
    let consentDocument = ORKConsentDocument()
    consentDocument.title = "RxBuddy Survey Consent"
    
    //TODO: consent sections
    let consentSectionTypes: [ORKConsentSectionType] = [
        .Overview,
        .DataGathering,
        .Privacy,
        .TimeCommitment,
        .StudySurvey,
        .StudyTasks,
    ]
    
    let consentSections: [ORKConsentSection] = consentSectionTypes.map { contentSectionType in
        let consentSection = ORKConsentSection(type: contentSectionType)
        consentSection.summary = "The RxBuddy survey is built to collects statistics on side effects and whether people regularly take their perscribed drugs"
        consentSection.content = "In this survey you will be asked 2 multiple choice questions and one text entry question"
        return consentSection
    }
    
    consentDocument.sections = consentSections
    
    //add signature to document
    consentDocument.addSignature(ORKConsentSignature(forPersonWithTitle: nil, dateFormatString: nil, identifier: "ConsentDocumentParticipantSignature"))
    //if consent document is finished change flag
    sharedLocalDB.setConsentTaken(true)
    return consentDocument
}
