//
//  AddMedicationsController.swift
//  RxBuddy
//
//  Created by Steven Huang on 2015-11-04.
//  Copyright (c) 2015 275g5. All rights reserved.
//

import Foundation
import ResearchKit

class AddMedicationsController: UIViewController {
    
    @IBOutlet var photo: UIImageView!
    @IBOutlet var dinField: UITextField!
    @IBOutlet var doctorNameField: UITextField!
    @IBOutlet var doctorContactField: UITextField!
    @IBOutlet weak var expiryDateField: UIDatePicker!
    var serializedImage : String = ""
    
    @IBAction
    func donePressed() {
        var dinText = dinField.text as NSString!
        var dinNum = Int(dinField.text!)
        var doctorNameText = doctorNameField.text
        var doctorContactText = doctorContactField.text
        var expiryDate = expiryDateField.date
        
        if !isValidDIN(dinText, dinNum: dinNum) {
            return
        }
        //every time a drug is added, check for negative interaction
        if let info = getInfoFromHealthCanada(dinText) {
            let name = info.valueForKey("name") as! String
            let strength = info.valueForKey("strength") as! String
            let ingredient = info.valueForKey("ingredient") as! String
            var newMed = MedicationItem(
                owner: sharedLocalDB.getUsername() as String,
                name: name,
                ingredient: ingredient,
                strength: strength,
                din: String(dinText),
                doctorName: doctorNameText!,
                doctorContact: doctorContactText!,
                expiryDate: expiryDate,
                reminder1: nil,
                reminder2: nil,
                reminder3: nil,
                taking: true,
                dateAdded: NSDate(),
                serializedImage: serializedImage,
                uuid: NSUUID().UUIDString)
            
            sharedLocalDB.addMedication(newMed)
            let currentMeds = sharedLocalDB.getAllCurrentMedications()
            if let interactions = getInteractions() {
                if let interactionsArrayTemp = interactions.valueForKey("multiInteractions") {
                    let interactionsArray = interactionsArrayTemp as! [NSDictionary]
                    for currInteraction in interactionsArray {
                        let drugA = currInteraction.valueForKey("object") as! String
                        let drugB = currInteraction.valueForKey("subject") as! String
                        let severity = currInteraction.valueForKey("severity") as! String
                        let severityID = currInteraction.valueForKey("severityId") as! Int
                        let text = currInteraction.valueForKey("text") as! String
                        
                        //print(drugA, drugB, severity, severityID, text)
                        var matched:MedicationItem? = nil
                        for med in currentMeds {
                            let firstWord = med.name.characters.split{$0 == " "}.map(String.init)[0].lowercaseString
                            if(drugA.lowercaseString.containsString(firstWord)) {
                                matched = med
                                break
                            }
                        }
                        let alert = UIAlertView()
                        alert.title = "Two of your medications have a negative interaction"
                        if let matchedMed = matched {
                            alert.message = text + "\n\n" + "Please contact " + matchedMed.doctorName + " at " + matchedMed.doctorContact
                        } else {
                            alert.message = text + "\n\n" + "Please contact your doctor for more information"
                        }
                        
                        alert.addButtonWithTitle("Ok")
                        alert.show()
                    }
                }
            }
            
            
        } else {
            return
        }
        

        //Prompt user to take the ReaserchKit Survey Consent Form after adding first drug
        if (!sharedLocalDB.isConsentTaken()){
            let alertController = UIAlertController(title: "Side Effects Survey Consent", message: "Would you like to complete a daily side effects survey?", preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
                self.navigationController?.popViewControllerAnimated(true)
            }
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                
                let taskViewController = ORKTaskViewController(task: ConsentTask, taskRunUUID: nil)
                taskViewController.delegate = self
                self.presentViewController(taskViewController, animated: true, completion: nil)
            }
            alertController.addAction(OKAction)
            
            self.presentViewController(alertController, animated: true) {
                // ...
            }
        }
        self.navigationController?.popViewControllerAnimated(true)
    
    }
    
    // scrolling to compensate for keyboard
    private func startObservingKeyboardEvents() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector:Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object:nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object:nil)
    }
    private func stopObservingKeyboardEvents() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    func keyboardWillShow(notification: NSNotification) {
        print("keyboard showing")
        if let userInfo = notification.userInfo {
            if let keyboardSize: CGSize = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue.size {
                let contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
                scrollView.contentInset = contentInset
                scrollView.scrollIndicatorInsets = contentInset
                scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y + keyboardSize.height)
            }
        }
    }
    func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsetsZero;
    }
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.contentSize.height = 1000
    }
    // end scrolling
    //cross refrence all medications to find negative interactions
    func getInteractions() -> NSDictionary? {
        var firstWordOfAllMeds = [String]()
        let currentMeds = sharedLocalDB.getAllCurrentMedications()
        if currentMeds.count < 2 {
            return nil
        }
        for med in currentMeds {
            let firstWord = med.name.characters.split{$0 == " "}.map(String.init)[0]
            firstWordOfAllMeds.append(firstWord)
        }
        let allMedsCSV = firstWordOfAllMeds.joinWithSeparator(",")
        
        let url:NSURL = NSURL(string: Constants.URL.GET_INTERACTIONS + allMedsCSV)!
        
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        
        var reponseError: NSError?
        var response: NSURLResponse?
        var urlData: NSData?
        do {
            urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
        } catch let error as NSError {
            reponseError = error
            urlData = nil
        }
        
        if ( urlData != nil ) {
            let res = response as! NSHTTPURLResponse!
            let responseData = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
            
            NSLog("Response code: %ld", res.statusCode);
            NSLog("Response data: %@",  responseData);
            let jsonData:NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers )) as! NSDictionary
            if (res.statusCode >= 200 && res.statusCode < 300) {
                return jsonData
            } else if(res.statusCode >= 400) {
                var error_msg:NSString
                
                if jsonData["msg"] as? NSString != nil {
                    error_msg = jsonData["msg"] as! NSString
                } else {
                    error_msg = "Unknown Error"
                }
                let alertView:UIAlertView = UIAlertView()
                alertView.title = "Unable to get drug interactions"
                alertView.message = error_msg as String
                alertView.delegate = self
                alertView.addButtonWithTitle("OK")
                alertView.show()
                
            }
        } else {
            let alertView:UIAlertView = UIAlertView()
            alertView.title = "Unable to get drug interactions"
            alertView.message = "Failed to connect to server"
            if let error = reponseError {
                alertView.message = (error.localizedDescription)
            }
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
        }
        return nil
    }
    //checks if DIN is valid
    func isValidDIN(dinString: NSString, dinNum: Int?) -> Bool {
        if dinString.length != 8 {
            let alert = UIAlertView()
            alert.title = "Invalid DIN"
            alert.message = "DIN must be 8 digits"
            alert.addButtonWithTitle("Ok")
            alert.show()
            return false
        }
        if dinNum == nil {
            let alert = UIAlertView()
            alert.title = "Invalid DIN"
            alert.message = "DIN must be a number"
            alert.addButtonWithTitle("Ok")
            alert.show()
            return false
        }
        return true
    }
    
    // getting the information from Health Canada
    func getInfoFromHealthCanada(din: NSString) -> NSDictionary? {
        let url:NSURL = NSURL(string: Constants.URL.GET_DIN_INFO + String(din))!
        
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        
        
        var reponseError: NSError?
        var response: NSURLResponse?
        
        var urlData: NSData?
        do {
            urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
        } catch let error as NSError {
            reponseError = error
            urlData = nil
        }
        
        if ( urlData != nil ) {
            let res = response as! NSHTTPURLResponse!
            let responseData = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
            
            NSLog("Response code: %ld", res.statusCode);
            NSLog("Response data: %@",  responseData);
            // fix twill unable to find form data
            if(res.statusCode == 500) {
                let alertView:UIAlertView = UIAlertView()
                alertView.title = "Unable to get DIN information"
                alertView.message = "Please try again"
                alertView.delegate = self
                alertView.addButtonWithTitle("OK")
                alertView.show()
                return nil
            }
            
            let jsonData:NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers )) as! NSDictionary
            if (res.statusCode >= 200 && res.statusCode < 300) {
                return jsonData
            } else if(res.statusCode >= 400) {
                var error_msg:NSString
                
                if jsonData["msg"] as? NSString != nil {
                    error_msg = jsonData["msg"] as! NSString
                } else {
                    error_msg = "Unknown Error"
                }
                let alertView:UIAlertView = UIAlertView()
                alertView.title = "Unable to get DIN information"
                alertView.message = error_msg as String
                alertView.delegate = self
                alertView.addButtonWithTitle("OK")
                alertView.show()
                
            }
        } else {
            let alertView:UIAlertView = UIAlertView()
            alertView.title = "Unable to get DIN information"
            alertView.message = "Failed to connect to server"
            if let error = reponseError {
                alertView.message = (error.localizedDescription)
            }
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
        }
        return nil
    }
    
    
    // add a year to the date, just 'cause
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        var components = NSDateComponents()
        components.setValue(1, forComponent: NSCalendarUnit.Year);
        expiryDateField.date = NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: NSDate(), options: NSCalendarOptions(rawValue: 0))!
        startObservingKeyboardEvents()
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        stopObservingKeyboardEvents()
    }
    func persistImage(image : UIImage) {
        
    }
}


extension AddMedicationsController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBAction func takephoto(sender: AnyObject) {
        let imagePickerActionSheet = UIAlertController(title: "Snap/Upload Photo",
            message: nil, preferredStyle: .ActionSheet)
        
        if UIImagePickerController.isSourceTypeAvailable(.Camera) {
            let cameraButton = UIAlertAction(title: "Take Photo",
                style: .Default) { (alert) -> Void in
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = .Camera
                    self.presentViewController(imagePicker,
                        animated: true,
                        completion: nil)
            }
            imagePickerActionSheet.addAction(cameraButton)
        }
        
        let libraryButton = UIAlertAction(title: "Choose Existing",
            style: .Default) { (alert) -> Void in
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .PhotoLibrary
                self.presentViewController(imagePicker,
                    animated: true,
                    completion: nil)
        }
        imagePickerActionSheet.addAction(libraryButton)
        
        let cancelButton = UIAlertAction(title: "Cancel",
            style: .Cancel) { (alert) -> Void in
        }
        imagePickerActionSheet.addAction(cancelButton)
        
        presentViewController(imagePickerActionSheet, animated: true,
            completion: nil)
    }
    
    // photo selected and displayed
    func imagePickerController(picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [String : AnyObject]) {
            let selectedPhoto = info[UIImagePickerControllerOriginalImage] as! UIImage
            let scaledImage = scaleImage(selectedPhoto, maxDimension: 400)
            photo.image = scaledImage
            let imageData = UIImagePNGRepresentation(scaledImage)
            serializedImage = imageData!.base64EncodedStringWithOptions([.Encoding64CharacterLineLength])
            dismissViewControllerAnimated(true, completion: {
                self.persistImage(scaledImage)
            })
    }
    
    func scaleImage(image: UIImage, maxDimension: CGFloat) -> UIImage {
        
        var scaledSize = CGSize(width: maxDimension, height: maxDimension)
        var scaleFactor: CGFloat
        
        if image.size.width > image.size.height {
            scaleFactor = image.size.height / image.size.width
            scaledSize.width = maxDimension
            scaledSize.height = scaledSize.width * scaleFactor
        } else {
            scaleFactor = image.size.width / image.size.height
            scaledSize.height = maxDimension
            scaledSize.width = scaledSize.height * scaleFactor
        }
        
        UIGraphicsBeginImageContext(scaledSize)
        image.drawInRect(CGRectMake(0, 0, scaledSize.width, scaledSize.height))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
}

extension AddMedicationsController : ORKTaskViewControllerDelegate {
    
    func taskViewController(taskViewController: ORKTaskViewController, didFinishWithReason reason: ORKTaskViewControllerFinishReason, error: NSError?) {
        //Handle results with taskViewController.result
        taskViewController.dismissViewControllerAnimated(true, completion: nil)
    }
//    func taskViewController(taskViewController: ORKTaskViewController, didFinishWithReason reason: ORKTaskViewControllerFinishReasonCancel, error: NSError?) {
//        //Handle results with taskViewController.result
//        taskViewController.dismissViewControllerAnimated(true, completion: nil)
//    }
    
}