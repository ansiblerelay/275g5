//
//  Constants.swift
//  RxBuddy
//
//  Created by Steven Huang on 2015-11-05.
//  Copyright (c) 2015 275g5. All rights reserved.
//

struct Constants {
    struct URL {
        static let ROOT = "https://stevenhuang.ca:5000"
        static let API = ROOT + "/rxbuddy/api/v1.0"
        
        // end points
        
        // post
        static let LOGIN = API + "/login"
        static let REGISTER = API + "/register"
        
        // get
        static let GET_DIN_INFO = API + "/get_din_info/"
        static let GET_FRIEND_CALENDARS = API + "/get_friend_calendars"
        static let GET_INTERACTIONS = API + "/medscape_get_interactions?dformat=names&dfield="
        static let GET_DESCRIPTION = API + "/mayo_get_description/"
        static let GET_SIDE_EFFECTS = API + "/mayo_get_side_effects/"
        static let GET_MAYO_INTERACTIONS = API + "/mayo_get_interactions/"
        //xbuddy/api/v1.0/medscape_get_interactions?dfield=cortisone,celebrex,haw,tobra\&dformat=names
    }
    
    
}