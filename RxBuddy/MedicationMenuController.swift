//
//  MedicationMenuController.swift
//  RxBuddy
//
//  Created by Noah Seltzer on 2015-11-20.
//  Copyright © 2015 275g5. All rights reserved.
//

import Foundation
import UIKit

class MedicationMenuController: UITableViewController{
    @IBOutlet var MedicationMenu: UITableView!
    var items: [String] = ["Current Medications", "Previous Medications", "Interactions Checker", "Add medication"]
    override func viewDidLoad(){
        
    }
    override func viewDidAppear(animated: Bool){
        super.viewDidAppear(animated)
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section:Int) ->Int{
        return items.count
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
        
        if(items[indexPath.row] == "Current Medications") {
            
            self.performSegueWithIdentifier("from_settings_to_login", sender: self)
        }
        if(items[indexPath.row] == "Remove all medications") {
            sharedLocalDB.removeAllMedications()
        }
    }
}

class ReportedSideEffectsVC: UITableViewController {
    var surveys: [String] = []

    override func viewDidLoad(){
        super.viewDidLoad()
    }
    override func viewWillAppear(animated: Bool){
        super.viewWillAppear(animated)
        surveys = sharedLocalDB.getResearchSurveys()
        tableView.reloadData()
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell!
        
        cell.textLabel?.text = "Survey " + String(indexPath.row + 1)
        cell.detailTextLabel?.text = ""
        
        return cell
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section:Int) ->Int{
        return surveys.count
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
        let seVC = self.storyboard?.instantiateViewControllerWithIdentifier("seDetailVC") as! ReportedSideEffectsDetailVC
        seVC.surveyText = surveys[indexPath.row]
        self.navigationController?.pushViewController(seVC, animated: true)
    }
}

class ReportedSideEffectsDetailVC: UIViewController {
    @IBOutlet weak var text: UITextView!
    @IBOutlet weak var navtitle: UINavigationItem!
    
    var surveyText: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewDidAppear(animated)
        text.text = surveyText!
    }
}