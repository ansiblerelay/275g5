//
//  MedicationsController.swift
//  RxBuddy
//
//  Created by Steven Huang on 2015-10-29.
//  Copyright (c) 2015 275g5. All rights reserved.
//
import UIKit
import EventKit

class MedicationsDetailController : UITableViewController, 
ReminderDelegate {
    // table view linked using storyboard
    
    var items: [String] = ["DIN", "Drug Name", "Active Ingredient", "Strength", "Expiry Date", "Doctor's Name", "Doctor's Contact", "View image", "More Information", "Side Effects", "Interactions"]
    var numReminders = 0
    var currentMed : MedicationItem?
    var actionLabels: [String] = ["End/Take medication placeholder", "Reset all reminders", "Permanently Delete Medication"]
    
    private func startObservingKeyboardEvents() {
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector:Selector("keyboardWillShow:"),
            name:UIKeyboardWillShowNotification,
            object:nil)
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector:Selector("keyboardWillHide:"),
            name:UIKeyboardWillHideNotification,
            object:nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewDidAppear(animated)
        actionLabels[0] = currentMed!.taking ? "Send to Previous Medications" : "Send to Current Medications"
        refreshList()
    }
    
    func refreshList() {
        numReminders = 0
        if currentMed!.reminder1 != MedicationItem.dummyDate {
            numReminders++
        }
        if currentMed!.reminder2 != MedicationItem.dummyDate {
            numReminders++
        }
        if currentMed!.reminder3 != MedicationItem.dummyDate {
            numReminders++
        }
        tableView.reloadData()
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return items.count
        case 1:
            // if 0 reminders, leave a cell to mention no reminders
            return numReminders == 0 ? 1 : numReminders
        case 2:
            return actionLabels.count
        default:
            return 0
        }
        
    }
    //handles medication reminders
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("medicationDetailCell") as UITableViewCell!
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                cell.detailTextLabel?.text = String(currentMed!.din)
            case 1:
                cell.detailTextLabel?.text = String(currentMed!.name)
            case 2:
                cell.detailTextLabel?.text = String(currentMed!.ingredient)
            case 3:
                cell.detailTextLabel?.text = String(currentMed!.strength)
            case 4:
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateStyle = NSDateFormatterStyle.FullStyle
                dateFormatter.dateFormat = "MMMM dd y"
                dateFormatter.timeZone = NSTimeZone.localTimeZone()
                cell.detailTextLabel?.text = dateFormatter.stringFromDate(currentMed!.expiryDate)
            case 5:
                cell.detailTextLabel?.text = String(currentMed!.doctorName)
            case 6:
                cell.detailTextLabel?.text = String(currentMed!.doctorContact)
            case 7:
                cell = tableView.dequeueReusableCellWithIdentifier("medicationDisclosureCell") as UITableViewCell!
                //cell.detailTextLabel?.text = String(currentMed!.doctorName)
                cell.detailTextLabel?.text = ""
            case 8:
                cell = tableView.dequeueReusableCellWithIdentifier("medicationDisclosureCell") as UITableViewCell!
                cell.detailTextLabel?.text = ""
            case 9:
                cell = tableView.dequeueReusableCellWithIdentifier("medicationDisclosureCell") as UITableViewCell!
                cell.detailTextLabel?.text = ""
            case 10:
                cell = tableView.dequeueReusableCellWithIdentifier("medicationDisclosureCell") as UITableViewCell!
                cell.detailTextLabel?.text = ""
            default:
                cell.detailTextLabel?.text = "undef"
            }
            cell.textLabel?.text = self.items[indexPath.row]
        case 1:
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateStyle = NSDateFormatterStyle.FullStyle
            dateFormatter.dateFormat = "hh:mm a"
            dateFormatter.timeZone = NSTimeZone.localTimeZone()
            cell = tableView.dequeueReusableCellWithIdentifier("medicationDisclosureCell") as UITableViewCell!
            cell.textLabel?.text = "Reminder " + String(indexPath.row + 1)
            switch indexPath.row {
            case 0:
                if numReminders == 0 {
                    cell.textLabel?.text = "You have no reminders added"
                    cell.detailTextLabel?.text = ""
                } else {
                    cell.detailTextLabel?.text = dateFormatter.stringFromDate(currentMed!.getIthReminder(0)!)
                    // need this to force redraw of detail text
                    cell.layoutSubviews()
                }
            case 1:
                cell.detailTextLabel?.text = dateFormatter.stringFromDate(currentMed!.getIthReminder(1)!)
            case 2:
                cell.detailTextLabel?.text = dateFormatter.stringFromDate(currentMed!.getIthReminder(2)!)
            default:
                cell.detailTextLabel?.text = "undef"
            }
        case 2:
            cell.textLabel?.text = self.actionLabels[indexPath.row]
            cell.detailTextLabel?.text = ""
        default:
            cell.textLabel?.text = "undef"
            cell.detailTextLabel?.text = "undef"
        }
        
        
        return cell
    }
    //grabs medications information from mayoclinic
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
        
        switch indexPath.section {
        case 0:
            switch items[indexPath.row] {
            case "View image":
                pushViewImageVC()
            case "More Information":
                pushMayoVCs(MayoController.MayoType.DESCRIPTION)
            case "Side Effects":
                pushMayoVCs(MayoController.MayoType.SIDE_EFFECTS)
            case "Interactions":
                pushMayoVCs(MayoController.MayoType.INTERACTIONS)
            default:
                1+1
            }
        case 1:
            switch indexPath.row {
            case 0:
                if numReminders > 0 {
                    pushModifyReminder(0)
                }
            case 1:
                pushModifyReminder(1)
            case 2:
                pushModifyReminder(2)
            default:
                print("Unhandled reminder press")
            }
        case 2:
            switch actionLabels[indexPath.row] {
            case "Send to Previous Medications":
                currentMed!.taking = false
                sharedLocalDB.updateMedication(currentMed!)
                self.navigationController?.popViewControllerAnimated(true)
            case "Send to Current Medications":
                currentMed!.taking = true
                sharedLocalDB.updateMedication(currentMed!)
                self.navigationController?.popViewControllerAnimated(true)
            case "Reset all reminders":
                resetAllReminders()
            case "Permanently Delete Medication":
                sharedLocalDB.removeMedication(currentMed!)
                self.navigationController?.popViewControllerAnimated(true)
            default:
                print("Unhandled Case 2 action labels -- misnamed?")
            }
            
        default:
            print("Unhandled section press")
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch(section) {
        case 0:
            return "Medication Details"
        case 1:
            return "Reminders"
        case 2:
            return "Actions"
        default:
            return ""
        }
    }
    
    // adding reminder
    @IBAction func addReminder() {
        let reminderVC = self.storyboard?.instantiateViewControllerWithIdentifier("AddReminderController") as! AddReminderController
        reminderVC.delegate = self
        self.navigationController?.pushViewController(reminderVC, animated: true)
    }
    //add image to medication
    func pushViewImageVC() {
        let imageVC = self.storyboard?.instantiateViewControllerWithIdentifier("DrugImageVC") as! DrugImageVC
        imageVC.med = currentMed
        self.navigationController?.pushViewController(imageVC, animated: true)
    }
    func pushMayoVCs(type : MayoController.MayoType) {
        let mayoVC = self.storyboard?.instantiateViewControllerWithIdentifier("mayoVC") as! MayoController
        mayoVC.type = type
        mayoVC.drug = currentMed
        self.navigationController?.pushViewController(mayoVC, animated: true)
    }
    func pushModifyReminder(index : Int) {
        let resetVC = self.storyboard?.instantiateViewControllerWithIdentifier("ResetReminderController") as! ResetReminderController
        resetVC.delegate = self
        resetVC.index = index
        self.navigationController?.pushViewController(resetVC, animated: true)
    }
    
    // adding notification of reminder
    func addNotification(date: NSDate) {
        registerForLocalNotifications()
        let userSettings: UIUserNotificationSettings = UIApplication.sharedApplication().currentUserNotificationSettings()!
        if(userSettings.types == UIUserNotificationType.None) {
            //self.alertFail()
            return
        }
        
        let localNotification = UILocalNotification()
        localNotification.alertTitle = "RxBuddy"
        localNotification.alertBody = "It is time to take " + currentMed!.name
        localNotification.alertAction = "open"
        localNotification.fireDate = date
        localNotification.repeatInterval = NSCalendarUnit.Day
        localNotification.timeZone = NSTimeZone.defaultTimeZone()
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.applicationIconBadgeNumber = UIApplication.sharedApplication().applicationIconBadgeNumber + 1
        localNotification.category = "reminderCategory"
        localNotification.userInfo = ["uuid": currentMed!.uuid]
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        
        
        //alertSuccess()
    }
    
    // reset all reminder in the list
    func resetAllReminders() {
        currentMed!.reminder1 = MedicationItem.dummyDate
        currentMed!.reminder2 = MedicationItem.dummyDate
        currentMed!.reminder3 = MedicationItem.dummyDate
        sharedLocalDB.updateMedication(currentMed!)
        refreshList()
        
        registerForLocalNotifications()
        let userSettings: UIUserNotificationSettings = UIApplication.sharedApplication().currentUserNotificationSettings()!
        if(userSettings.types == UIUserNotificationType.None) {
            //self.alertFail()
            return
        }
        
        alertResetSuccess()
    }
    
    // delete all of reminder
    @IBAction func Cancelreminder(){
        var app:UIApplication = UIApplication.sharedApplication()
        for oneEvent in app.scheduledLocalNotifications! {
            var notification = oneEvent as! UILocalNotification
            let userInfoCurrent = notification.userInfo! as! [String:AnyObject]
            let uid = userInfoCurrent["uuid"]! as! String
            if uid == currentMed!.uuid {
                //Cancelling local notification
                app.cancelLocalNotification(notification)
                break;
            }
        }
    }
    
    func registerForLocalNotifications() {
        // actions
        let reminderActionConfirm = UIMutableUserNotificationAction()
        reminderActionConfirm.identifier = "Confirm"
        reminderActionConfirm.title = "Confirm"
        reminderActionConfirm.activationMode = UIUserNotificationActivationMode.Background
        reminderActionConfirm.destructive = false
        reminderActionConfirm.authenticationRequired = false
        
        let reminderActionLater = UIMutableUserNotificationAction()
        reminderActionLater.identifier = "Later"
        reminderActionLater.title = "Later"
        reminderActionLater.activationMode = UIUserNotificationActivationMode.Background
        reminderActionLater.destructive = true
        reminderActionLater.authenticationRequired = false
        
        let reminderActionSkip = UIMutableUserNotificationAction()
        reminderActionSkip.identifier = "Skip"
        reminderActionSkip.title = "Skip"
        reminderActionSkip.activationMode = UIUserNotificationActivationMode.Background
        reminderActionSkip.destructive = true
        reminderActionSkip.authenticationRequired = false
        
        let drugReminderCategory = UIMutableUserNotificationCategory()
        drugReminderCategory.identifier = "reminderCategory"
        drugReminderCategory.setActions([reminderActionConfirm, reminderActionLater, reminderActionSkip], forContext: .Default)
        drugReminderCategory.setActions([reminderActionConfirm, reminderActionLater], forContext: .Minimal)
        
        // prompt
        let notificationSettings = UIUserNotificationSettings(forTypes: [UIUserNotificationType.Alert, UIUserNotificationType.Sound, UIUserNotificationType.Badge], categories: Set(arrayLiteral: drugReminderCategory))
        UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
    }
    
    func alertSuccess() {
        let alert = UIAlertController(title: "Success", message: "Reminder added", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func alertResetSuccess() {
        let alert = UIAlertController(title: "Success", message: "Your reminders have been reset", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func alertFail() {
        let alert = UIAlertController(title: "Failed", message: "Could not add the reminder. Please check RxBuddy's notification settings", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func didFinishAddingReminder(controller: AddReminderController) {
        print("Popped date: ", terminator: "")
        print(controller.timeToTake.date)
        if currentMed!.reminder1 == MedicationItem.dummyDate {
            currentMed!.reminder1 = controller.timeToTake.date
            // addNotification(NSDate().dateByAddingTimeInterval(2))
            addNotification(controller.timeToTake.date)
        } else if currentMed!.reminder2 == MedicationItem.dummyDate {
            currentMed!.reminder2 = controller.timeToTake.date
            addNotification(controller.timeToTake.date)
        } else if currentMed!.reminder3 == MedicationItem.dummyDate {
            currentMed!.reminder3 = controller.timeToTake.date
            addNotification(controller.timeToTake.date)
        } else {
            return
        }
        // Persist this change
        sharedLocalDB.updateMedication(currentMed!)
    }
    
    func didFinishResetingReminder(controller: ResetReminderController) {
        print("Popped date: ", terminator: "")
        print(controller.timeToTake.date)
        
        let newDate = controller.cancel ? MedicationItem.dummyDate : controller.timeToTake.date
        
        // get the selected reminder
        let ithReminder = currentMed!.getIthReminder(controller.index)
        let index = currentMed!.getIndexOfReminder(ithReminder)
        if index == 0 {
            currentMed!.reminder1 = newDate
        } else if index == 1 {
            currentMed!.reminder2 = newDate
        } else if index == 2 {
            currentMed!.reminder3 = newDate
        }
        if controller.cancel {
        } else {
            // ResetNotification(NSDate().dateByAddingTimeInterval(2))
            addNotification(newDate)
        }
        // Persist this change
        sharedLocalDB.updateMedication(currentMed!)
    }
    
    
    
}

protocol ReminderDelegate {
    func didFinishAddingReminder(controller: AddReminderController)
    func didFinishResetingReminder(controller: ResetReminderController)
}

// UI Buttom for Add Reminder
class AddReminderController : UIViewController {
    @IBOutlet weak var timeToTake: UIDatePicker!
    
    var delegate: ReminderDelegate! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewDidAppear(animated)
        timeToTake.timeZone = NSTimeZone.defaultTimeZone()
    }
    
    @IBAction func onDoneClicked() {
        self.navigationController?.popViewControllerAnimated(true)
        delegate.didFinishAddingReminder(self)
    }
}

// UI Buttom for Reset Reminder
class ResetReminderController : UIViewController {
    @IBOutlet weak var timeToTake: UIDatePicker!
    
    var delegate: ReminderDelegate! = nil
    var index: Int = 0
    var cancel: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewDidAppear(animated)
        timeToTake.timeZone = NSTimeZone.defaultTimeZone()
    }
    
    @IBAction func onDoneClicked() {
        self.navigationController?.popViewControllerAnimated(true)
        delegate.didFinishResetingReminder(self)
    }
    
    @IBAction func onCancelClicked() {
        cancel = true
        onDoneClicked()
    }
}

class MayoController : UIViewController {
    @IBOutlet weak var navtitle: UINavigationItem!
    @IBOutlet weak var text: UITextView!
    
    var type: MayoType = MayoType.NONE
    var drug: MedicationItem? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch type {
        case .DESCRIPTION:
            navtitle.title = "Information"
            do {
                text.attributedText = try NSAttributedString(data: getDescription().dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
            } catch {
                print(error)
            }
        case .SIDE_EFFECTS:
            navtitle.title = "Side Effects"
            do {
                text.attributedText = try NSAttributedString(data: getSideEffects().dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
            } catch {
                print(error)
            }
        case .INTERACTIONS:
            navtitle.title = "Interactions"
            do {
                text.attributedText = try NSAttributedString(data: getInteractions().dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
            } catch {
                print(error)
            }
        default:
            print("Unexpected mayotype")
        }
    }
    override func viewWillAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func getDescription() -> String {
        let firstWordOfDrug = drug!.name.characters.split{$0 == " "}.map(String.init)[0]
        let url:NSURL = NSURL(string: Constants.URL.GET_DESCRIPTION + firstWordOfDrug)!
        
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        
        var reponseError: NSError?
        var response: NSURLResponse?
        var urlData: NSData?
        do {
            urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
        } catch let error as NSError {
            reponseError = error
            urlData = nil
        }
        
        if ( urlData != nil ) {
            let res = response as! NSHTTPURLResponse!
            let responseData = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
            
            NSLog("Response code: %ld", res.statusCode);
            NSLog("Response data: %@",  responseData);
            let jsonData:NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers )) as! NSDictionary
            if (res.statusCode >= 200) {
                if jsonData["success"] as! Int == 1 {
                    return jsonData["description"] as! String
                } else {
                    var error_msg:NSString
                    
                    if jsonData["msg"] as? NSString != nil {
                        error_msg = jsonData["msg"] as! NSString
                    } else {
                        error_msg = "Unknown Error"
                    }
                    let alertView:UIAlertView = UIAlertView()
                    alertView.title = "Unable to get drug interactions"
                    alertView.message = error_msg as String
                    alertView.delegate = self
                    alertView.addButtonWithTitle("OK")
                    alertView.show()
                }
            }
        } else {
            let alertView:UIAlertView = UIAlertView()
            alertView.title = "Unable to get drug interactions"
            alertView.message = "Failed to connect to server"
            if let error = reponseError {
                alertView.message = (error.localizedDescription)
            }
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
        }
        self.navigationController!.popToRootViewControllerAnimated(true)
        return ""
    }
    func getSideEffects() -> String {
        let firstWordOfDrug = drug!.name.characters.split{$0 == " "}.map(String.init)[0]
        let url:NSURL = NSURL(string: Constants.URL.GET_SIDE_EFFECTS + firstWordOfDrug)!
        
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        
        var reponseError: NSError?
        var response: NSURLResponse?
        var urlData: NSData?
        do {
            urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
        } catch let error as NSError {
            reponseError = error
            urlData = nil
        }
        
        if ( urlData != nil ) {
            let res = response as! NSHTTPURLResponse!
            let responseData = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
            
            NSLog("Response code: %ld", res.statusCode);
            NSLog("Response data: %@",  responseData);
            let jsonData:NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers )) as! NSDictionary
            if (res.statusCode >= 200) {
                if jsonData["success"] as! Int == 1 {
                    return jsonData["side_effects"] as! String
                } else {
                    var error_msg:NSString
                    
                    if jsonData["msg"] as? NSString != nil {
                        error_msg = jsonData["msg"] as! NSString
                    } else {
                        error_msg = "Unknown Error"
                    }
                    let alertView:UIAlertView = UIAlertView()
                    alertView.title = "Unable to get drug side effects"
                    alertView.message = error_msg as String
                    alertView.delegate = self
                    alertView.addButtonWithTitle("OK")
                    alertView.show()
                }
            }
        } else {
            let alertView:UIAlertView = UIAlertView()
            alertView.title = "Unable to get drug side effects"
            alertView.message = "Failed to connect to server"
            if let error = reponseError {
                alertView.message = (error.localizedDescription)
            }
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
        }
        self.navigationController!.popToRootViewControllerAnimated(true)
        return ""
    }
    func getInteractions() -> String {
        let firstWordOfDrug = drug!.name.characters.split{$0 == " "}.map(String.init)[0]
        let url:NSURL = NSURL(string: Constants.URL.GET_MAYO_INTERACTIONS + firstWordOfDrug)!
        
        let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        
        var reponseError: NSError?
        var response: NSURLResponse?
        var urlData: NSData?
        do {
            urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
        } catch let error as NSError {
            reponseError = error
            urlData = nil
        }
        
        if ( urlData != nil ) {
            let res = response as! NSHTTPURLResponse!
            let responseData = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
            
            NSLog("Response code: %ld", res.statusCode);
            NSLog("Response data: %@",  responseData);
            let jsonData:NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers )) as! NSDictionary
            if (res.statusCode >= 200) {
                if jsonData["success"] as! Int == 1 {
                    return jsonData["interactions"] as! String
                } else {
                    var error_msg:NSString
                    
                    if jsonData["msg"] as? NSString != nil {
                        error_msg = jsonData["msg"] as! NSString
                    } else {
                        error_msg = "Unknown Error"
                    }
                    let alertView:UIAlertView = UIAlertView()
                    alertView.title = "Unable to get drug interactions"
                    alertView.message = error_msg as String
                    alertView.delegate = self
                    alertView.addButtonWithTitle("OK")
                    alertView.show()
                }
            }
        } else {
            let alertView:UIAlertView = UIAlertView()
            alertView.title = "Unable to get drug interactions"
            alertView.message = "Failed to connect to server"
            if let error = reponseError {
                alertView.message = (error.localizedDescription)
            }
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
        }
        self.navigationController!.popToRootViewControllerAnimated(true)
        return ""
    }
    enum MayoType {
        case DESCRIPTION
        case SIDE_EFFECTS
        case INTERACTIONS
        case NONE
    }
}

class DrugImageVC : UIViewController {
    @IBOutlet weak var photo: UIImageView!
    var med : MedicationItem? = nil
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let decodedData = NSData(base64EncodedString: med!.serializedImage, options: [.IgnoreUnknownCharacters])
        let decodedImage = UIImage(data: decodedData!)
        photo.image = decodedImage
    }
}
