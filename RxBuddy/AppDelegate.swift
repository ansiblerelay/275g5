//
//  AppDelegate.swift
//  RxBuddy
//
//  Created by Steven Huang on 2015-10-11.
//  Copyright (c) 2015 275g5. All rights reserved.
//

import UIKit
import ResearchKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        //whenever the app starts, it checks to see if there are any expired medications and notifys the user
        if(sharedLocalDB.isLoggedIn()){
        var expireddrugs = sharedLocalDB.getExpiredDrugs()
        if (expireddrugs.count > 0){
            var expiredstr = String(expireddrugs[0])
            for var i = 1; i < expireddrugs.count; ++i{
                expiredstr = expiredstr + ", " + expireddrugs[i]
            }
            var expiredalert = UIAlertView()
            expiredalert.title = "These Medications are Expired"
            expiredalert.message = expiredstr
            expiredalert.delegate = self
            expiredalert.addButtonWithTitle("OK")
            expiredalert.show()
        }
        }
        return true
    }

    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        print("notification setting: ", terminator: "")
        print(notificationSettings.types.rawValue)
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        // Point for handling the local notification when the app is open.
        // Showing reminder details in an alertview
        //UIAlertView(title: notification.alertTitle, message: notification.alertBody, delegate: nil, cancelButtonTitle: "OK").show()
        print("recv local notif: ", terminator: "")
        print(notification.alertBody)
        let alertController = UIAlertController(title: notification.alertTitle, message: notification.alertBody, preferredStyle: .Alert)
        if(notification.category == "survey") {
            //check if drugs are taken today and reset flag
            if(sharedLocalDB.isDrugTakenToday() == true){
                sharedLocalDB.addTakenDay(NSDate())
            }
            else{
                sharedLocalDB.addNotTakenDay(NSDate())
            }
            sharedLocalDB.setDrugTodayTaken(true)
            let surveyConfirmAction = UIAlertAction(title: "Take Survey", style: .Default){ (action) in
                //open researchkit survey
                let taskViewController = ORKTaskViewController(task: SurveyTask, taskRunUUID: nil)
                taskViewController.delegate = self
                let rootController = application.windows[0].rootViewController
                if let navigationController = rootController as? UITabBarController {
                    navigationController.presentViewController(taskViewController, animated: true, completion: nil)
                }
                
            }
            alertController.addAction(surveyConfirmAction)
            
        } else{
            //reminder actions
            let firstAction = UIAlertAction(title: "Confirm", style: .Default) { (action) in
                UIApplication.sharedApplication().cancelLocalNotification(notification)
            }
            let secondAction = UIAlertAction(title: "Later", style: .Default) { (action) in
                notification.fireDate = NSDate().dateByAddingTimeInterval(5*60)
                UIApplication.sharedApplication().scheduleLocalNotification(notification)
            }
            let thirdAction = UIAlertAction(title: "Skip", style: .Default) { (action) in
                sharedLocalDB.setDrugTodayTaken(false)
                UIApplication.sharedApplication().cancelLocalNotification(notification)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
                sharedLocalDB.setDrugTodayTaken(false)
                UIApplication.sharedApplication().cancelLocalNotification(notification)
            }
            
            alertController.addAction(firstAction)
            alertController.addAction(secondAction)
            alertController.addAction(thirdAction)
            alertController.addAction(cancelAction)
        }
        
        let rootController = application.windows[0].rootViewController
        if let navigationController = rootController as? UITabBarController {
            navigationController.presentViewController(alertController, animated: true, completion: nil)
        }
        
        
        // tab bar buttons
        let tabBarController = self.window!.rootViewController as! UITabBarController
        let tabBar = tabBarController.tabBar as UITabBar
        let tabBarItem1 = tabBar.items![0] as UITabBarItem
        let tabBarItem2 = tabBar.items![1] as UITabBarItem
        let tabBarItem3 = tabBar.items![2] as UITabBarItem
        _ = tabBar.items![3] as UITabBarItem
        tabBarItem1.selectedImage = UIImage(named: "FirstSelectedImage")
        tabBarItem2.selectedImage = UIImage(named: "SecondSelectedImage")
        tabBarItem3.selectedImage = UIImage(named: "ThirdSelectedImage")
        tabBarItem3.selectedImage = UIImage(named: "ForthSelectedImage")
        
    }


    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forLocalNotification notification: UILocalNotification, completionHandler: () -> Void) {

        if identifier == "ShowDetails" {
            UIAlertView(title: notification.alertTitle, message: notification.alertBody, delegate: nil, cancelButtonTitle: "OK").show()
            return
        } else if identifier == "Later" {
            // remind again in 5 minutes
            notification.fireDate = NSDate().dateByAddingTimeInterval(5*60)
            UIApplication.sharedApplication().scheduleLocalNotification(notification)
        } else if identifier == "Confirm" {
            //sharedLocalDB.removeMedicationByUUID(notification.userInfo!["uuid"] as! String!)
        }
        completionHandler()
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        // reset badges
        if application.applicationIconBadgeNumber > 0 {
            application.applicationIconBadgeNumber = 0
        }
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    

}

class MainTab: UITabBarController {
    
    override func viewDidLoad() {
        
        let tabBar = self.tabBar
        
        let homeSelectImage: UIImage! = UIImage(named: "FirstSelectedImage")?.imageWithRenderingMode(.AlwaysOriginal)
        let qaSelectImage: UIImage! = UIImage(named: "SecondSelectedImage")?.imageWithRenderingMode(.AlwaysOriginal)
        let mySelectImage: UIImage! = UIImage(named: "ThirdSelectedImage")?.imageWithRenderingMode(.AlwaysOriginal)
        let meSelectImage: UIImage! = UIImage(named: "ForthSelectedImage")?.imageWithRenderingMode(.AlwaysOriginal)
        
        (tabBar.items![0] ).selectedImage = homeSelectImage
        (tabBar.items![1] ).selectedImage = qaSelectImage
        (tabBar.items![2] ).selectedImage = mySelectImage
        (tabBar.items![3] ).selectedImage = meSelectImage
        
//        tabBar.tintColor = UIColor.greenColor()
        
        }
}
extension AppDelegate : ORKTaskViewControllerDelegate {
    
    func taskViewController(taskViewController: ORKTaskViewController, didFinishWithReason reason: ORKTaskViewControllerFinishReason, error: NSError?) {
        //Handle results with taskViewController.result
        taskViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
