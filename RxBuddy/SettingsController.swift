//
//  SettingsController.swift
//  RxBuddy
//
//  Created by Steven Huang on 2015-10-29.
//  Copyright (c) 2015 275g5. All rights reserved.
//

import UIKit
import MessageUI
import ResearchKit

let exportKey = "Export Medication Data"
let importKey = "Import Medication Data"

class SettingsController: UITableViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet var settingsTable: UITableView!
    
    var items: [String] = [exportKey, importKey, "Logout", "Adverse Drug Reactions",  "Add an RxBuddy", "Survey Consent Form", "Research Survey", "Terms of Use & Disclaimers"]
    
    override func viewDidLoad() {
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    // distinguish two kind of table view cell
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = settingsTable.dequeueReusableCellWithIdentifier("settingDetailCell") as UITableViewCell!
        if(items[indexPath.row] == "Add an RxBuddy") {
            cell = settingsTable.dequeueReusableCellWithIdentifier("settingDisclosureCell") as UITableViewCell!
        }
        
        cell.textLabel?.text = self.items[indexPath.row]
        cell.detailTextLabel?.text = ""
        
        return cell
    }
    
    // email
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["your_email@you.com"])
        mailComposerVC.setSubject("RxBuddy - Your Medications")
        mailComposerVC.setMessageBody(sharedLocalDB.asJSON(), isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // identity some special row item
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
        switch items[indexPath.row] {
        case exportKey:
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(mailComposeViewController, animated: true, completion: nil)
            } else {
                self.showSendMailErrorAlert()
            }
        case importKey:
            break
        default:
            break
        }
        if(items[indexPath.row] == "Logout") {
            sharedLocalDB.setLoggedOut()
            print("You have logged out")
            self.performSegueWithIdentifier("from_settings_to_login", sender: self)
        }
        if(items[indexPath.row] == "Remove all medications") {
            sharedLocalDB.removeAllMedications()
        }
        if(items[indexPath.row] == "Remove all Survey Results") {
            sharedLocalDB.removeAllSurveys()
        }
        
        if(items[indexPath.row] == "Survey Consent Form") {
            let taskViewController = ORKTaskViewController(task: ConsentTask, taskRunUUID: nil)
            taskViewController.delegate = self
            presentViewController(taskViewController, animated: true, completion: nil)
        }
        
        if(items[indexPath.row] == "Information About Adverse Drug Reactions") {
            let VC = storyboard?.instantiateViewControllerWithIdentifier("adr") as! ADRinfoViewController
            navigationController?.pushViewController(VC, animated: true)
        }
        
        if(items[indexPath.row] == "Terms of Use & Disclaimers") {
            let disclaimers = storyboard?.instantiateViewControllerWithIdentifier("TermsOfUse") as! TermsOfUseViewController
            navigationController?.pushViewController(disclaimers, animated: true)
        }
        
        if(items[indexPath.row] == "Research Survey") {
            let allMeds = sharedLocalDB.getAllCurrentMedications()
            if allMeds.count == 0 {
                return
            }
            let taskViewController = ORKTaskViewController(task: SurveyTask, taskRunUUID: nil)
            taskViewController.delegate = self
            presentViewController(taskViewController, animated: true, completion: nil)
        }
        
        
        
    }
}

class FriendsController: UITableViewController {
    @IBOutlet var friendsTable: UITableView!
    
    var friends: [String] = []
    
    override func viewDidLoad() {
        
        var username = sharedLocalDB.getUsername() as String
        var password = sharedLocalDB.getPassword() as String
        var combined = Constants.URL.GET_FRIEND_CALENDARS + "?username=" + username + "&password=" + password
        var url:NSURL = NSURL(string: combined)!
        var request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
        
        var reponseError: NSError?
        var response: NSURLResponse?
        var urlData: NSData?
        do {
            urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse:&response)
        } catch var error as NSError {
            reponseError = error
            urlData = nil
        }
        if ( urlData != nil ) {
            let res = response as! NSHTTPURLResponse!
            let responseData = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
            
            NSLog("Response code: %ld", res.statusCode);
            NSLog("Response data: %@",  responseData);
            
            if (res.statusCode >= 200 && res.statusCode < 300) {
                
                var error: NSError?
                
                let jsonData:NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers )) as! NSDictionary
                
                let success:NSInteger = jsonData.valueForKey("success") as! NSInteger
                
                //[jsonData[@"success"] integerValue];
                
                NSLog("Success: %ld", success);
                
                if(success == 1)
                {
                    NSLog("Get friend success");
                    if jsonData["Add an RxBuddy"] as? NSArray != nil {
                        
                    }
                    
                    sharedLocalDB.setUsername(username)
                    sharedLocalDB.setLoggedIn()
                } else {
                    var error_msg:NSString
                    
                    if jsonData["msg"] as? NSString != nil {
                        error_msg = jsonData["msg"] as! NSString
                    } else {
                        error_msg = "Unknown Error"
                    }
                    var alertView:UIAlertView = UIAlertView()
                    alertView.title = "Sign in Failed!"
                    alertView.message = error_msg as String
                    alertView.delegate = self
                    alertView.addButtonWithTitle("OK")
                    alertView.show()
                    
                }
                
            } else {
                var alertView:UIAlertView = UIAlertView()
                alertView.title = "Sign in Failed!"
                alertView.message = "Connection Failed"
                alertView.delegate = self
                alertView.addButtonWithTitle("OK")
                alertView.show()
            }
        } else {
            var alertView:UIAlertView = UIAlertView()
            alertView.title = "Sign in Failed!"
            alertView.message = "Connection Failure"
            if let error = reponseError {
                alertView.message = (error.localizedDescription)
            }
            alertView.delegate = self
            alertView.addButtonWithTitle("OK")
            alertView.show()
        }
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friends.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = friendsTable.dequeueReusableCellWithIdentifier("friendsDetailCell") as UITableViewCell!
        
        cell.textLabel?.text = self.friends[indexPath.row]
        cell.detailTextLabel?.text = ""
        
        return cell
    }
    
}

extension SettingsController : ORKTaskViewControllerDelegate {
    
    func taskViewController(taskViewController: ORKTaskViewController, didFinishWithReason reason: ORKTaskViewControllerFinishReason, error: NSError?) {
        if reason == ORKTaskViewControllerFinishReason.Discarded || reason == ORKTaskViewControllerFinishReason.Failed {
            taskViewController.dismissViewControllerAnimated(true, completion: nil)
            return
        }
        
        //Handle results with taskViewController.result
        var formattedText = "Medications Taken\n"
        
        print(taskViewController.result.description, "\n")
        if let drugsSelectedResultQuestion = taskViewController.result.resultForIdentifier("MEQuestionStep") {
            let drugsSelectedResult = (drugsSelectedResultQuestion as! ORKStepResult).resultForIdentifier("MEQuestionStep") as! ORKChoiceQuestionResult
            for drugName in drugsSelectedResult.choiceAnswers! {
                print(drugName)
                formattedText += (drugName as! String) + "\n"
            }
        } else {
            return
        }
        
        formattedText += "\nSide Effects Experienced\n"
        let sideEffectsResultQuestion = taskViewController.result.resultForIdentifier("SEQuestionStep") as! ORKStepResult
        let sideEffectsResult = sideEffectsResultQuestion.resultForIdentifier("SEQuestionStep") as! ORKChoiceQuestionResult
        for seIndex in sideEffectsResult.choiceAnswers! {
            let sideEffect = (sideEffectChoices[seIndex as! Int]).text
            formattedText += sideEffect + "\n"
        }
        formattedText += "\nNotes\n"
        let notesResultQuestion = taskViewController.result.resultForIdentifier("OSEQuestionStep") as! ORKStepResult
        let notesResult = notesResultQuestion.resultForIdentifier("OSEQuestionStep") as! ORKTextQuestionResult
        if let notes = notesResult.answer {
            formattedText += notes as! String
        } else {
            formattedText += "No notes recorded"
        }
        print(formattedText)
        sharedLocalDB.addResearchSurvey(formattedText)
        taskViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

public var ConsentTask: ORKOrderedTask {
    
    var steps = [ORKStep]()
    
    //TODO: Add VisualConsentStep
    var consentDocument = ConsentDocument
    let visualConsentStep = ORKVisualConsentStep(identifier: "VisualConsentStep", document: consentDocument)
    steps += [visualConsentStep]
    
    //TODO: Add ConsentReviewStep
    let signature = consentDocument.signatures!.first as ORKConsentSignature!
    
    let reviewConsentStep = ORKConsentReviewStep(identifier: "ConsentReviewStep", signature: signature, inDocument: consentDocument)
    
    reviewConsentStep.text = "Review Consent!"
    reviewConsentStep.reasonForConsent = "Consent to join study"
    
    steps += [reviewConsentStep]
    
    return ORKOrderedTask(identifier: "ConsentTask", steps: steps)
}

class ADRinfoViewController : UIViewController{
    override func viewDidLoad() {
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    
}

class TermsOfUseViewController : UIViewController{
    override func viewDidLoad() {
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}


