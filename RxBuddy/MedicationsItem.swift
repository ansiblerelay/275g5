//
//  MedicationsModel.swift
//  RxBuddy
//
//  Created by Jerry Li on 2015-11-03.
//  Copyright (c) 2015 275g5. All rights reserved.
//

import UIKit

public struct MedicationItem {
    public var owner: String
    public var name: String
    public var ingredient: String
    public var strength: String
    public var din: String
    public var doctorName: String
    public var doctorContact: String
    public var taking: Bool
    public var dateAdded: NSDate
    public var serializedImage: String
    public var uuid: String
    
    public var expiryDate: NSDate
    public var reminder1: NSDate?
    public var reminder2: NSDate?
    public var reminder3: NSDate?
    
    public init(
        owner: String,
        name: String,
        ingredient: String,
        strength: String,
        din: String,
        doctorName: String,
        doctorContact: String,
        expiryDate: NSDate,
        reminder1: NSDate?,
        reminder2: NSDate?,
        reminder3: NSDate?,
        taking: Bool,
        dateAdded: NSDate,
        serializedImage: String,
        uuid: String) {
            self.owner = owner
            self.name = name
            self.ingredient = ingredient
            self.strength = strength
            self.din = din
            self.doctorName = doctorName
            self.doctorContact = doctorContact
        
            self.expiryDate = expiryDate
            self.reminder1 = reminder1
            self.reminder2 = reminder2
            self.reminder3 = reminder3
            
            self.taking = taking
            self.dateAdded = dateAdded
            self.serializedImage = serializedImage
            self.uuid = uuid
    }
    static var dummyDate = NSDate(timeIntervalSinceReferenceDate: 0)
    
    func getIthReminder(ith : Int) -> NSDate? {
        let temp = [reminder1, reminder2, reminder3]
        // -1 for offset 0
        var counter = -1
        for rem in temp {
            if rem != MedicationItem.dummyDate {
                counter++
            }
            if counter == ith {
                return rem
            }
        }
        return nil
    }
    
    func getIndexOfReminder(reminder : NSDate?) -> Int {
        let temp = [reminder1, reminder2, reminder3]
        var counter = 0;
        for rem in temp {
            if reminder == rem {
                return counter
            }
            counter++
        }
        return -1
    }
}