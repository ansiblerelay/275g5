//
//  SurveyTask.swift
//  RxBuddy
//
//  Created by Paul Vu on 2015-11-19.
//  Copyright © 2015 275g5. All rights reserved.
//

import Foundation
import ResearchKit

public let sideEffectChoices = [
    ORKTextChoice(text: "Nausea", value: 0),
    ORKTextChoice(text: "Constipation", value: 1),
    ORKTextChoice(text: "Muscle Ache", value: 2),
    ORKTextChoice(text: "Skin Reaction (Rash, Blemishes, ect.)", value: 3),
    ORKTextChoice(text: "Drowsiness", value: 4),
    ORKTextChoice(text: "Joint Pain", value: 5),
    ORKTextChoice(text: "Dizzyness", value: 6),
    ORKTextChoice(text: "Depression", value: 8),
    ORKTextChoice(text: "Soreness", value: 9),
    ORKTextChoice(text: "Headache", value: 10),
    ORKTextChoice(text: "Insomnia", value: 11),
    ORKTextChoice(text: "Sweating", value: 12),
    ORKTextChoice(text: "Feaver", value: 13),
    ORKTextChoice(text: "Constipation", value: 15),
    ORKTextChoice(text: "Asthma", value: 16),
    ORKTextChoice(text: "Skin Irritation", value: 17)
    
]
public var SurveyTask: ORKOrderedTask{
    var steps = [ORKStep]()
    let instructionStep = ORKInstructionStep(identifier: "IntroStep")
    instructionStep.title = "Question 1"
    instructionStep.text = "If you are experiencing any side effects that you would like to report, please press 'get started'"
    steps += [instructionStep]
    //SE = Side effect
    let MEQuestionStepTitle = "Check all of the medications you remembered to take today"
    var textChoices = [ORKTextChoice]()
    var counter = 0
    let allMeds = sharedLocalDB.getAllCurrentMedications()
    for meds in allMeds {
        textChoices.append(ORKTextChoice(text: meds.name, value: meds.name))
        counter++
    }
    let MEAnswerFormat: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormatWithStyle(.MultipleChoice, textChoices: textChoices)
    let MEQuestionStep = ORKQuestionStep(identifier: "MEQuestionStep", title: MEQuestionStepTitle, answer: MEAnswerFormat)
    steps += [MEQuestionStep]
    let SEQuestionStepTitle = "Please tap any of the side effects that you have experienced today"
    
    let SEAnswerFormat: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormatWithStyle(.MultipleChoice, textChoices: sideEffectChoices)
    let SEQuestionStep = ORKQuestionStep(identifier: "SEQuestionStep", title: SEQuestionStepTitle, answer: SEAnswerFormat)
    steps += [SEQuestionStep]
    let OSEAnswerFormat = ORKTextAnswerFormat(maximumLength: 1000)
    OSEAnswerFormat.multipleLines = true
    let OSEQuestionStepTitle = "Are you experiencing any other side effects?"
    let OSEQuestionStep = ORKQuestionStep(identifier: "OSEQuestionStep", title: OSEQuestionStepTitle, answer: OSEAnswerFormat)
    steps += [OSEQuestionStep]
    
    
    
    let summaryStep = ORKCompletionStep(identifier: "SummaryStep")
    summaryStep.title = "Thank you for your time"
    summaryStep.text = "Please answer the survey again if you feel any new side effects"
    steps += [summaryStep]
    
    //dizzyness, nausea, itching, rash, dry mouth, dry skin, drowsyness, muscle pain,
    return ORKOrderedTask(identifier: "SurveyTask", steps: steps)
}