//
//  CalendarController.swift
//  RxBuddy
//
//  Created by Steven Huang on 2015-10-18.
//  Copyright (c) 2015 275g5. All rights reserved.
//

import UIKit
import EventKit



class CalendarController : UIViewController {
    // MARK: - Properties
    @IBOutlet weak var calendarView: CVCalendarView!
    @IBOutlet weak var menuView: CVCalendarMenuView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var daysOutSwitch: UISwitch!
    
    var shouldShowDaysOut = true
    var animationFinished = true
    
    var selectedDay:DayView!
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        monthLabel.text = CVDate(date: NSDate()).globalDescription
    }
    
    @IBAction func removeCircleAndDot(sender: AnyObject) {
        if let dayView = selectedDay {
            calendarView.contentController.removeCircleLabel(dayView)
            calendarView.contentController.removeDotViews(dayView)
        }
    }
    
    @IBAction func refreshMonth(sender: AnyObject) {
        calendarView.contentController.refreshPresentedMonth()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        calendarView.commitCalendarViewUpdate()
        menuView.commitMenuViewUpdate()
    }
}



extension CalendarController: CVCalendarViewDelegate, CVCalendarMenuViewDelegate {
    
    
    func presentationMode() -> CalendarMode {
        return .MonthView
    }
    
    //method to set first weekday
    func firstWeekday() -> Weekday {
        return .Sunday
    }
    
    
    
    func shouldShowWeekdaysOut() -> Bool {
        return shouldShowDaysOut
    }
    
    func shouldAnimateResizing() -> Bool {
        return true // Default value is true
    }
    
    func didSelectDayView(dayView: CVCalendarDayView, animationDidFinish: Bool) {
        print("\(dayView.date.commonDescription) is selected!")
        selectedDay = dayView
    }
    //CVCalendar day view customization method
    func presentedDateUpdated(date: CVDate) {
        if monthLabel.text != date.globalDescription && self.animationFinished {
            let updatedMonthLabel = UILabel()
            updatedMonthLabel.textColor = monthLabel.textColor
            updatedMonthLabel.font = monthLabel.font
            updatedMonthLabel.textAlignment = .Center
            updatedMonthLabel.text = date.globalDescription
            updatedMonthLabel.sizeToFit()
            updatedMonthLabel.alpha = 0
            updatedMonthLabel.center = self.monthLabel.center
            
            let offset = CGFloat(48)
            updatedMonthLabel.transform = CGAffineTransformMakeTranslation(0, offset)
            updatedMonthLabel.transform = CGAffineTransformMakeScale(1, 0.1)
            
            UIView.animateWithDuration(0.35, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                self.animationFinished = false
                self.monthLabel.transform = CGAffineTransformMakeTranslation(0, -offset)
                self.monthLabel.transform = CGAffineTransformMakeScale(1, 0.1)
                self.monthLabel.alpha = 0
                
                updatedMonthLabel.alpha = 1
                updatedMonthLabel.transform = CGAffineTransformIdentity
                
                }) { _ in
                    
                    self.animationFinished = true
                    self.monthLabel.frame = updatedMonthLabel.frame
                    self.monthLabel.text = updatedMonthLabel.text
                    self.monthLabel.transform = CGAffineTransformIdentity
                    self.monthLabel.alpha = 1
                    updatedMonthLabel.removeFromSuperview()
            }
            
            self.view.insertSubview(updatedMonthLabel, aboveSubview: self.monthLabel)
        }
    }
    
    func topMarker(shouldDisplayOnDayView dayView: CVCalendarDayView) -> Bool {
        return true
    }
    //method to choose which days should have a dot appear on them.
    func dotMarker(shouldShowOnDayView dayView: CVCalendarDayView) -> Bool {
        let month = dayView.date.month
        let day = dayView.date.day
        let allDays = sharedLocalDB.getDaysTaken() + sharedLocalDB.getDaysNotTaken()
        //dummy data
        //let allDays = ["2015/11/01","2015/11/02","2015/11/03","2015/11/04","2015/11/05","2015/11/06","2015/11/07","2015/11/08","2015/11/09","2015/11/10","2015/11/11","2015/11/12","2015/11/13","2015/11/14","2015/11/15","2015/11/16","2015/11/17","2015/11/18","2015/11/19","2015/11/20","2015/11/21","2015/11/22","2015/11/23","2015/11/24","2015/11/25","2015/11/26","2015/11/27","2015/11/28","2015/11/28","2015/11/29","2015/11/30"]
        for var i = 0; i < allDays.count; ++i{
            let dotmonth = allDays[i].substringWithRange(Range<String.Index>(start:allDays[i].startIndex.advancedBy(5), end: allDays[i].endIndex.advancedBy(-3)))
            
            let dotday = allDays[i].substringWithRange(Range<String.Index>(start:allDays[i].endIndex.advancedBy(-2), end: allDays[i].endIndex))
            
            if(month == Int(dotmonth)){
                if(day == Int(dotday)){
                    print("HALLELUJAH the day is", dotday, "the month is", dotmonth)
                    return true
                }
            }
        }
        return false
        
    }
    //method to choose colour of dots
        func dotMarker(colorOnDayView dayView: CVCalendarDayView) -> [UIColor] {
        let month = dayView.date.month
        let day = dayView.date.day
        let red = UIColor(red: 255, green: 0, blue: 0, alpha: 1)
        let green = UIColor(red: 0, green: 255, blue: 0, alpha: 1)
        let white = UIColor(red: 255, green: 255, blue: 255, alpha: 1)
        let daysTaken = sharedLocalDB.getDaysTaken()
        let daysNotTaken = sharedLocalDB.getDaysNotTaken()
            //dummy data
            //let daysTaken = ["2015/11/01","2015/11/02","2015/11/03","2015/11/07","2015/11/08","2015/11/10","2015/11/11","2015/11/12","2015/11/13","2015/11/14","2015/11/15","2015/11/16","2015/11/17","2015/11/18","2015/11/19","2015/11/20","2015/11/25","2015/11/26","2015/11/27","2015/11/28","2015/11/28","2015/11/29","2015/11/30"]
            //let daysNotTaken = ["2015/11/04","2015/11/05","2015/11/06","2015/11/09","2015/11/21","2015/11/22","2015/11/23","2015/11/24"]
            for var i = 0; i < daysTaken.count; ++i{
                let dotmonth = daysTaken[i].substringWithRange(Range<String.Index>(start:daysTaken[i].startIndex.advancedBy(5), end: daysTaken[i].endIndex.advancedBy(-3)))
                let dotday = daysTaken[i].substringWithRange(Range<String.Index>(start:daysTaken[i].endIndex.advancedBy(-2), end: daysTaken[i].endIndex))
                if (month == Int(dotmonth)){
                    if(day == Int(dotday)){
                        //print("HALLELUJAH the day is", dotday, "the month is", dotmonth)
                        return [green]
                    }
                }
            }
            for var i = 0; i < daysNotTaken.count; ++i{
                let dotmonth = daysNotTaken[i].substringWithRange(Range<String.Index>(start:daysNotTaken[i].startIndex.advancedBy(5), end: daysNotTaken[i].endIndex.advancedBy(-3)))
                let dotday = daysNotTaken[i].substringWithRange(Range<String.Index>(start:daysNotTaken[i].endIndex.advancedBy(-2), end: daysNotTaken[i].endIndex))
                if (month == Int(dotmonth)){
                    if(day == Int(dotday)){
                        return [red]
                    }
                }
            }
        return [white]
    }
    
    func dotMarker(shouldMoveOnHighlightingOnDayView dayView: CVCalendarDayView) -> Bool {
        return true
    }
    
    func dotMarker(sizeOnDayView dayView: DayView) -> CGFloat {
        return 13
    }
    
    
    func weekdaySymbolType() -> WeekdaySymbolType {
        return .Short
    }
    
    func preliminaryView(viewOnDayView dayView: DayView) -> UIView {
        let circleView = CVAuxiliaryView(dayView: dayView, rect: dayView.bounds, shape: CVShape.Circle)
        circleView.fillColor = .colorFromCode(0xCCCCCC)
        return circleView
    }
    
    func preliminaryView(shouldDisplayOnDayView dayView: DayView) -> Bool {
        if (dayView.isCurrentDay) {
            return true
        }
        return false
    }
    //incomplete feature
    //func supplementaryView(viewOnDayView dayView: DayView) -> UIView {
//        let π = M_PI
//        
//        let ringSpacing: CGFloat = 3.0
//        let ringInsetWidth: CGFloat = 1.0
//        let ringVerticalOffset: CGFloat = 1.0
//        var ringLayer: CAShapeLayer!
//        let ringLineWidth: CGFloat = 4.0
//        let ringLineColour: UIColor = .blueColor()
//        
//        let newView = UIView(frame: dayView.bounds)
//        
//        let diameter: CGFloat = (newView.bounds.width) - ringSpacing
//        let radius: CGFloat = diameter / 2.0
//        
//        let rect = CGRectMake(newView.frame.midX-radius, newView.frame.midY-radius-ringVerticalOffset, diameter, diameter)
//        
//        ringLayer = CAShapeLayer()
//        newView.layer.addSublayer(ringLayer)
//        
//        ringLayer.fillColor = nil
//        ringLayer.lineWidth = ringLineWidth
//        ringLayer.strokeColor = ringLineColour.CGColor
//        
//        let ringLineWidthInset: CGFloat = CGFloat(ringLineWidth/2.0) + ringInsetWidth
//        let ringRect: CGRect = CGRectInset(rect, ringLineWidthInset, ringLineWidthInset)
//        let centrePoint: CGPoint = CGPointMake(ringRect.midX, ringRect.midY)
//        let startAngle: CGFloat = CGFloat(-π/2.0)
//        let endAngle: CGFloat = CGFloat(π * 2.0) + startAngle
//        let ringPath: UIBezierPath = UIBezierPath(arcCenter: centrePoint, radius: ringRect.width/2.0, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        
//        ringLayer.path = ringPath.CGPath
//        ringLayer.frame = newView.layer.bounds
        
        //return newView
  //  }
    //related to above
    func supplementaryView(shouldDisplayOnDayView dayView: DayView) -> Bool {
        if (Int(arc4random_uniform(3)) == 1) {
            return true
        }
        
        return false
    }
}


// MARK: - CVCalendarViewAppearanceDelegate

extension CalendarController: CVCalendarViewAppearanceDelegate {
    func dayLabelPresentWeekdayInitallyBold() -> Bool {
        return false
    }
    
    func spaceBetweenDayViews() -> CGFloat {
        return 2
    }
}



extension CalendarController {
    @IBAction func switchChanged(sender: UISwitch) {
        if sender.on {
            calendarView.changeDaysOutShowingState(false)
            shouldShowDaysOut = true
        } else {
            calendarView.changeDaysOutShowingState(true)
            shouldShowDaysOut = false
        }
    }
    
    @IBAction func todayMonthView() {
        calendarView.toggleCurrentDayView()
    }
    
    /// Switch to WeekView mode.
    @IBAction func toWeekView(sender: AnyObject) {
        calendarView.changeMode(.WeekView)
    }
    
    /// Switch to MonthView mode.
    @IBAction func toMonthView(sender: AnyObject) {
        calendarView.changeMode(.MonthView)
    }
    
    @IBAction func loadPrevious(sender: AnyObject) {
        calendarView.loadPreviousView()
    }
    
    
    @IBAction func loadNext(sender: AnyObject) {
        calendarView.loadNextView()
    }
}



extension CalendarController {
    func toggleMonthViewWithMonthOffset(offset: Int) {
        let calendar = NSCalendar.currentCalendar()
        //        let calendarManager = calendarView.manager
        let components = Manager.componentsForDate(NSDate()) // from today
        
        components.month += offset
        
        let resultDate = calendar.dateFromComponents(components)!
        
        self.calendarView.toggleViewWithDate(resultDate)
    }
    
    func didShowNextMonthView(date: NSDate)
    {
        //        let calendar = NSCalendar.currentCalendar()
        //        let calendarManager = calendarView.manager
        let components = Manager.componentsForDate(date) // from today
        
        print("Showing Month: \(components.month)")
    }
    
    
    func didShowPreviousMonthView(date: NSDate)
    {
        //        let calendar = NSCalendar.currentCalendar()
        //        let calendarManager = calendarView.manager
        let components = Manager.componentsForDate(date) // from today
        
        print("Showing Month: \(components.month)")
    }
    
}
