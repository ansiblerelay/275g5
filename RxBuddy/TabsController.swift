//
//  TabsController.swift
//  RxBuddy
//
//  Created by Steven Huang on 2015-10-18.
//  Copyright (c) 2015 275g5. All rights reserved.
//

import UIKit

class TabsController: UITabBarController {
    //Register app for daily Survey notificaitons
    func registerSurveyNotifications() {
        // actions
        let reminderActionConfirm = UIMutableUserNotificationAction()
        reminderActionConfirm.identifier = "Take Survey"
        reminderActionConfirm.title = "Take Survey"
        reminderActionConfirm.activationMode = UIUserNotificationActivationMode.Background
        reminderActionConfirm.destructive = false
        reminderActionConfirm.authenticationRequired = false
        
        let reminderActionLater = UIMutableUserNotificationAction()
        reminderActionLater.identifier = "Later"
        reminderActionLater.title = "Later"
        reminderActionLater.activationMode = UIUserNotificationActivationMode.Background
        reminderActionLater.destructive = true
        reminderActionLater.authenticationRequired = false
        
        let reminderActionSkip = UIMutableUserNotificationAction()
        reminderActionSkip.identifier = "Skip"
        reminderActionSkip.title = "Skip"
        reminderActionSkip.activationMode = UIUserNotificationActivationMode.Background
        reminderActionSkip.destructive = true
        reminderActionSkip.authenticationRequired = false
        
        let drugReminderCategory = UIMutableUserNotificationCategory()
        drugReminderCategory.identifier = "survey"
        drugReminderCategory.setActions([reminderActionConfirm, reminderActionLater, reminderActionSkip], forContext: .Default)
        drugReminderCategory.setActions([reminderActionConfirm, reminderActionLater], forContext: .Minimal)
        
        // prompt
        let notificationSettings = UIUserNotificationSettings(forTypes: [UIUserNotificationType.Alert, UIUserNotificationType.Sound, UIUserNotificationType.Badge], categories: Set(arrayLiteral: drugReminderCategory))
        UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
    }
    
    
    
    //Add a daily reminder for Survey
    func addSurveyNotification() {
        registerSurveyNotifications()
        let test : NSDateComponents = NSDateComponents()
        test.year = 2015
        test.month = 07
        test.hour = 21
        test.minute = 0
        test.second = 0
        test.timeZone = NSTimeZone.defaultTimeZone()
        
        let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let date: NSDate = calendar.dateFromComponents(test)!
        
        let dailyNotification : UILocalNotification = UILocalNotification()
        dailyNotification.alertTitle = "Medication Survey"
        dailyNotification.alertBody = "Would you like to participate in a survey for adverse drug reaction research? "
        dailyNotification.alertAction = " Yes "
        dailyNotification.fireDate = date
        dailyNotification.repeatInterval = NSCalendarUnit.Day
        dailyNotification.timeZone = NSTimeZone.defaultTimeZone()
        dailyNotification.soundName = UILocalNotificationDefaultSoundName
        dailyNotification.category = "survey"
        UIApplication.sharedApplication().scheduleLocalNotification(dailyNotification)
        
    }
    

    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        // won't switch to login if tried here
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        sharedLocalDB.synchronize()
        if (sharedLocalDB.isLoggedIn()) {
            print("in tabs logged in")
            self.selectedIndex = 0
            if(sharedLocalDB.isConsentTaken()){
            addSurveyNotification()
            
            print("survey called")
            }
            
        } else {
            print("in tabs not logged in")
            self.performSegueWithIdentifier("goto_login", sender: self)
        }
        
        
    }
}



