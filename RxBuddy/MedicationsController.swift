//
//  MedicationsController.swift
//  RxBuddy
//
//  Created by Steven Huang on 2015-10-29.
//  Copyright (c) 2015 275g5. All rights reserved.
//
import UIKit
import EventKit

class MedicationsController : UITableViewController{
    // table view linked using storyboard
    
    var medications: [MedicationItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewDidAppear(animated)
        refreshList()
    }
    
    func refreshList() {
        medications = sharedLocalDB.getAllCurrentMedications()
        tableView.reloadData()
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medications.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("medicationCell") as UITableViewCell!
        let item:MedicationItem = self.medications[indexPath.row]
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = String(item.din)
    
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "from_medications_to_detail" {
            if let destination = segue.destinationViewController as? MedicationsDetailController {
                if let index = tableView.indexPathForSelectedRow?.row {
                    //print("before segue: ", medications[index])
                    destination.currentMed = medications[index]
                }
            }
        }
    }
    
}

class PreviousMedicationsController : UITableViewController {
    // table view linked using storyboard
    
    var medications: [MedicationItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewDidAppear(animated)
        refreshList()
    }
    
    func refreshList() {
        medications = sharedLocalDB.getAllPreviousMedications()
        tableView.reloadData()
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medications.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("medicationCell") as UITableViewCell!
        let item:MedicationItem = self.medications[indexPath.row]
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = String(item.din)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("You selected cell #\(indexPath.row)!")
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "from_medications_to_detail" {
            if let destination = segue.destinationViewController as? MedicationsDetailController {
                if let index = tableView.indexPathForSelectedRow?.row {
                    //print("before segue: ", medications[index])
                    destination.currentMed = medications[index]
                }
            }
        }
    }
    
}