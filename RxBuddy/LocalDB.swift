//
//  LocalDB.swift
//  RxBuddy
//
//  Created by Steven Huang on 2015-10-30.
//  Copyright (c) 2015 275g5. All rights reserved.
//

import Foundation
import UIKit

public class LocalDB {
    let loggedInKey = "IS_LOGGED_IN"
    let userPasswordKey = "USER_PASSWORD"
    let userNameKey = "USER_NAME"
    let userEmailKey = "USER_EMAIL"
    let medicationsKey = "MEDICATIONS_KEY"
    let researchSurveyKey = "RESEARCH_SURVEY_KEY"
    let RKConsentTakenKey = "CONSENT_TAKEN_KEY"
    let daysTakenKey = "DAYS_TAKEN_KEY"
    let daysNotTakenKey = "DAYS_NOT_TAKEN_KEY"
    let drugTodayTakenKey = "DRUG_TODAY_TAKEN_KEY"
    
    private let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    public init() {
        
    }
    
    static var instance:LocalDB? = nil
    public static func getInstance() -> LocalDB {
        if let check = instance {
            return instance!
        } else {
            instance = LocalDB()
            return instance!
        }
    }
    
    func isLoggedIn() -> Bool {
        return prefs.integerForKey(loggedInKey) == 1
    }
    
    func setLoggedIn() {
        prefs.setInteger(1, forKey: loggedInKey)
    }
    
    func setLoggedOut() {
        prefs.setInteger(0, forKey: loggedInKey)
    }
    
    func setUsername(username : NSString) {
        prefs.setObject(username, forKey: userNameKey)
    }
    func setPassword(password : NSString) {
        prefs.setObject(password, forKey: userPasswordKey)
    }
    
    func getUsername() -> NSString {
        return prefs.objectForKey(userNameKey) as! NSString
    }
    func getPassword() -> NSString {
        return prefs.objectForKey(userPasswordKey) as! NSString
    }
    //is ResearchKit consent document finished?
    func isConsentTaken() -> Bool {
        return prefs.boolForKey((getUsername() as String) + RKConsentTakenKey)
    }
    //call function with argument false if user forgets to take drug
    func setDrugTodayTaken(taken : Bool){
        prefs.setBool(taken, forKey: (getUsername() as String) + drugTodayTakenKey)
    }
    
    //To be called when Consent Document is finished
    func setConsentTaken(consent : Bool){
        prefs.setBool(consent, forKey: (getUsername() as String) + RKConsentTakenKey)
    }
    //to be called when deciding whether to add todays date to "daysNotTaken" or "daysTaken"
    func isDrugTakenToday() -> Bool {
        return prefs.boolForKey((getUsername() as String) + drugTodayTakenKey)
    }
    //add date to list of days when user remembers to take all drugs
    func addTakenDay(date:NSDate){
        let currentDay = NSDate()
        let stringFormatter = NSDateFormatter()
        stringFormatter.dateFormat = "yyyy/MM/dd"
        let dateString = stringFormatter.stringFromDate(currentDay)
        var currDates = ""
        if let currDatesTemp = prefs.objectForKey((getUsername() as String) + daysTakenKey) {
            currDates = currDatesTemp as! String
            currDates += "," + dateString
            print("added", dateString, "to dates taken")
        } else {
            currDates = dateString
            print("added", dateString, "to dates taken")
        }
        
        prefs.setObject(currDates, forKey: (getUsername() as String) + daysTakenKey)
        
    }
    //add date to list of days when user does not remember to take all drugs
    func addNotTakenDay(date:NSDate){
        let currentDay = date
        let stringFormatter = NSDateFormatter()
        stringFormatter.dateFormat = "yyyy/MM/dd"
        let dateString = stringFormatter.stringFromDate(currentDay)
        
        var currDates = ""
        if let currDatesTemp = prefs.objectForKey((getUsername() as String) + daysTakenKey) {
            currDates = currDatesTemp as! String
            
            currDates += "," + dateString
        } else {
            currDates = dateString
        }
        
        prefs.setObject(currDates, forKey: (getUsername() as String) + daysNotTakenKey)
        
    }
    //outputs days taken as array of strings
    func getDaysTaken()->[String]{
        if let currDatesTemp = prefs.objectForKey((getUsername() as String) + daysTakenKey) {
            let allDaysTakenString = currDatesTemp as! String  //get days taken
            let allDaysTakenStringArr = allDaysTakenString.characters.split { $0 == "," }.map(String.init)
            return allDaysTakenStringArr
        }
        return [String]()
    }
    //outputs days not taken as array of strings
    func getDaysNotTaken()->[String]{
        if let currDatesTemp = prefs.objectForKey((getUsername() as String) + daysNotTakenKey) {
            let allDaysNotTakenString = currDatesTemp as! String //get days not taken
            let allDaysNotTakenStringArr = allDaysNotTakenString.characters.split { $0 == "," }.map(String.init)
            return allDaysNotTakenStringArr
        }
        return [String]()
    }
    
    // get all medications belonging to current user
    func getAllMedications() -> [MedicationItem] {
        //prefs.removeObjectForKey(medicationsKey)
        // if dictionary exists, use it, otherwise make empty dict
        let dict = prefs.dictionaryForKey(medicationsKey) ?? [:]
        let items = Array(dict.values)
        var mapped = items.map(
            {
                MedicationItem(
                    owner: $0["owner"] as! String,
                    name: $0["name"] as! String,
                    ingredient: $0["ingredient"] as! String,
                    strength: $0["strength"] as! String,
                    din: $0["din"] as! String,
                    doctorName: $0["doctorName"] as! String,
                    doctorContact: $0["doctorContact"] as! String,
                    expiryDate: $0["expiryDate"] as! NSDate,
                    reminder1: $0["reminder1"] as! NSDate?,
                    reminder2: $0["reminder2"] as! NSDate?,
                    reminder3: $0["reminder3"] as! NSDate?,
                    taking: $0["taking"] as! Bool,
                    dateAdded: $0["dateAdded"] as! NSDate,
                    serializedImage: $0["serializedImage"] as! String,
                    uuid: $0["uuid"] as! String!
                )
            }
        )
        var filtered = [MedicationItem]()
        let currentOwner = sharedLocalDB.getUsername()
        for item in mapped {
            if item.owner == currentOwner {
                filtered.append(item)
            }
        }
        return filtered
    }
    //return array of users current medication items
    func getAllCurrentMedications() -> [MedicationItem] {
        let items = getAllMedications()
        var currentMeds = [MedicationItem]()
        for item in items {
            if item.taking {
                currentMeds.append(item)
            }
        }
        return currentMeds
    }
    //return array of users previous medication items
    func getAllPreviousMedications() -> [MedicationItem] {
        let items = getAllMedications()
        var previousMeds = [MedicationItem]()
        for item in items {
            if !item.taking {
                previousMeds.append(item)
            }
        }
        return previousMeds
    }
    //add medication to users list of medications
    public func addMedication(item: MedicationItem) {
        // get existing dict, otherwise get empty dict
        var dict = prefs.dictionaryForKey(medicationsKey) ?? Dictionary()
        dict[item.uuid] = [
            "owner": item.owner,
            "name": item.name,
            "ingredient": item.ingredient,
            "strength": item.strength,
            "din": item.din,
            "doctorName": item.doctorName,
            "doctorContact": item.doctorContact,
            "expiryDate": item.expiryDate,
            "reminder1": item.reminder1 == nil ? MedicationItem.dummyDate : item.reminder1!,
            "reminder2": item.reminder2 == nil ? MedicationItem.dummyDate : item.reminder2!,
            "reminder3": item.reminder3 == nil ? MedicationItem.dummyDate : item.reminder3!,
            "taking": item.taking,
            "serializedImage": item.serializedImage,
            "dateAdded": item.dateAdded,
            "uuid": item.uuid] // save with uuid as key
        prefs.setObject(dict, forKey: medicationsKey)
    }
    // Convenience method - works since will overwrite previouse item due to same uuid
    public func updateMedication(item: MedicationItem) {
        addMedication(item)
    }
    //return array of all drugs whos expiry date matches the current date
    public func getExpiredDrugs()->[String]{
        let medArray = sharedLocalDB.getAllCurrentMedications()
        let formatter = NSDateFormatter()
        let currdate = NSDate()
        formatter.dateFormat = "yyyy/MM/dd"
        var expireddrugs = [String]()
        let currDateStr = formatter.stringFromDate(currdate)
        for var i = 0 ; i < medArray.count; ++i{
            let ExpDate = medArray[i].expiryDate
            let expDatestr = formatter.stringFromDate(ExpDate)
            if(expDatestr == currDateStr){
                expireddrugs.append(medArray[i].name)
            }
            
            
        }
        return expireddrugs
    }
    
    public func removeMedication(item: MedicationItem) {
        // cancel notifications of all matching uuid
        //for notification in UIApplication.sharedApplication().scheduledLocalNotifications as [UILocalNotification]! {
        //    if(let item = notification.userInfo!["uuid"] as! String == item.uuid) {
        //        UIApplication.sharedApplication().cancelLocalNotification(notification)
        //        break
        //    }
        //}
        
        if var dict = prefs.dictionaryForKey(medicationsKey) {
            dict.removeValueForKey(item.uuid)
            prefs.setObject(dict, forKey: medicationsKey)
        }
    }
    func removeMedicationByUUID(itemUUID: String) {
        for notification in UIApplication.sharedApplication().scheduledLocalNotifications as [UILocalNotification]! {
            if(notification.userInfo!["uuid"] as! String == itemUUID) {
                UIApplication.sharedApplication().cancelLocalNotification(notification)
                break
            }
        }
        
        if var dict = prefs.dictionaryForKey(medicationsKey) {
            dict.removeValueForKey(itemUUID)
            prefs.setObject(dict, forKey: medicationsKey)
        }
    }
    func removeAllMedications() {
        let meds = getAllMedications()
        for med in meds {
            removeMedication(med)
        }
    }
    func removeAllSurveys() {
        prefs.setObject(nil, forKey: (getUsername() as! String) + researchSurveyKey)
    }
    //saves users completed researchkit survey
    func addResearchSurvey(surveyText: String) {
        var surveys = ""
        if let surveysTemp = prefs.objectForKey((getUsername() as! String) + researchSurveyKey) {
            surveys = surveysTemp as! String
            surveys += "|" + surveyText
        } else {
            surveys = surveyText
        }
        prefs.setObject(surveys, forKey: (getUsername() as! String) + researchSurveyKey)
    }
    //get all survey results as array of strings
    func getResearchSurveys() -> [String] {
        if let surveysTemp = prefs.objectForKey((getUsername() as! String) + researchSurveyKey) {
            let surveys = surveysTemp as! String
            let surveysArray = surveys.characters.split{ $0 == "|" }.map(String.init)
            return surveysArray
        }
        return [String]()
    }
    
    func asJSON() -> String {
        let jsonDict = prefs.dictionaryRepresentation()
        //let jsonData:NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers )) as! NSDictionary
        //print(jsonDict)
        return jsonDict.description
    }
    
    func synchronize() {
        prefs.synchronize()
    }
}
//create an instance of localdb specific to the user
let sharedLocalDB = LocalDB()