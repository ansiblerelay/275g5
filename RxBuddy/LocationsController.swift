//
//  LocationsController.swift
//  RxBuddy
//
//  Created by Steven Huang on 2015-10-18.
//  Copyright (c) 2015 275g5. All rights reserved.
//

import UIKit
import MapKit
import AddressBookUI

class LocationsController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var buttons: UISegmentedControl!
    
    let regionRadius: CLLocationDistance = 2000
    var locManager: CLLocationManager = CLLocationManager();
    
    var annotation:MKAnnotation!
    var localSearchRequest:MKLocalSearchRequest!
    var localSearch:MKLocalSearch!
    var localSearchResponse:MKLocalSearchResponse!
    var error:NSError!
    var pointAnnotation:MKPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    
    var matchingItems: [MKMapItem] = [MKMapItem]()

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        locManager = CLLocationManager()
        locManager.delegate = self
        locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
    }
    override func viewDidDisappear(animated: Bool) {
        locManager.stopUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("DIDUPDATELOCATIONS")
        centerMapOnLocation(locations[0])
    }
    
    func locationManager(manager: CLLocationManager, didStartMonitoringForRegion region: CLRegion) {
        print("started monitoring")
    }
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .AuthorizedAlways:
            print("authorized")
            mapView.showsUserLocation = true
        case .AuthorizedWhenInUse:
            print("authorized when in use")
            mapView.showsUserLocation = true
        case .Denied:
            print("denied")
        case .NotDetermined:
            print("not determined")
        case .Restricted:
            print("restricted")
        }
    }
    
    //zoom in on GPS location
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    // http://www.techotopia.com/index.php/Working_with_MapKit_Local_Search_in_iOS_8_and_Swift
    //find all pramacies and clinics
    func searchFor(query: String) {
        let annotationsToRemove = mapView.annotations.filter{$0 !== self.mapView.userLocation}
        mapView.removeAnnotations(annotationsToRemove)
        matchingItems.removeAll()
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = query
        request.region = mapView.region
        
        let search = MKLocalSearch(request: request)

        search.startWithCompletionHandler({(response: MKLocalSearchResponse?,
            error: NSError?) in

            if let myError = error {
                print("Error occured in search: \(myError.localizedDescription)")
            } else if response!.mapItems.count == 0 {
                print("No matches found")
            } else {
                print("Matches found")
                for item in response!.mapItems {
                    //println("Name = \(item.name)")
                    //println("Phone = \(item.phoneNumber)")
                    
                    let itemAddressDictionary:NSDictionary = item.placemark.addressDictionary!
                    //println(itemAddressDictionary)
                    
                    
                    self.matchingItems.append(item as MKMapItem)
                    //println("Matching items = \(self.matchingItems.count)")
                    
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = item.placemark.coordinate
                    if let number = item.phoneNumber {
                        annotation.title = item.name as String! + " " + (number as String!)
                    }
                    
                    annotation.subtitle = ABCreateStringWithAddressDictionary(itemAddressDictionary as [NSObject : AnyObject], false)
                    self.mapView.addAnnotation(annotation)
                    
                }
            }
        })
    }
    //switch between displaying clinics and pharmacies
    @IBAction func onButtonClick() {
        switch buttons.selectedSegmentIndex {
        case 0:
            searchFor("clinic")
        case 1:
            searchFor("pharmacy")
        default:
            print("unhandled click")
        }
    }
}